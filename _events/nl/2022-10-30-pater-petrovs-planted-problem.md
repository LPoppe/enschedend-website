---
title: Pater Petrov's Planted Problem
image: banner_09_pater_petrov.png
form: https://docs.google.com/forms/d/e/1FAIpQLSdRXFsOOJcsXDDUxkSZIJBswqkM4hUKxFZ2yTnxFjaPB_wGmA/viewform
soldout: true
---

Ben jij klaar voor griezelige zombies, een mysterieuze tuin en (naar geruchten) een vervloekt klooster? Schrijf je dan nu is voor het nieuwe avontuur van EnscheD&D! Het wordt een spannend verhaal vol bovennatuurlijke verschijnselen, planten die je een kopje kleiner proberen te maken en genoeg undead om de clerics zoet te houden.

**Datum & tijd: 30 oktober 2022, 12.00-17.30u bij het Volbert in Enschede!**

Entree is 7,50 voor spelers.
