---
title: Slime Time
image: banner_04_slime_time.jpg
---

Een nieuw avontuur, vol slijmerige grotten, onverwachte schatten en vreemde NPC's waarin niets is wat het lijkt... Durf jij het avontuur aan? Geef je nu op voor de EnscheD&D Gameday!

Mail naar [{{ site.email }}](mailto:{{ site.email }}) om je op te geven. Je krijgt van daaruit een bevestiging toegestuurd.

Meedoen als groep? Dat kan ook! Geef bij je aanmelding aan bij wie je aan tafel wilt, en het wordt geregeld. Een perfecte gelegenheid om ook je vrienden, nichtje, buurman of oudtante te introduceren aan de vreugde van samen D&D'en!
