---
title: Saint Sofia and the 7 Spirits
image: banner_16_saint_sofia.png
form: https://docs.google.com/forms/d/e/1FAIpQLSelJgLaNcpNlWfi-Ipa67ww2xoKWuMogO3kKSUIkeC5P-EGhg/viewform
soldout: true
---

**Goed nieuws: de inschrijvingen voor EnscheD&D zijn geopend! Voor het
eerst spreiden we het evenement over 2 dagen. Het brengt wel wat extra
gepuzzel met zich mee over het aantal tafels dat we kunnen bedienen,
maar we rekenen erop dat het goed komt! 🔢 En nu... Zijn jullie klaar om
met ons mee te gaan op een nieuw, koninklijk avontuur? 👑**

Het verhaal speelt zich af in het kleine handelsstadje O'rethere, bekend
om zijn appelboomgaarden en prachtige wilde dieren en natuur. Maar diep
onder de plaatselijke kerk ontwaakt er iets uit een diepe slaap.
Schatten en rijkdom liggen voor het oprapen, maar dat komt niet zonder
gevaar. Pak je lantaarns en pikhouwelen, graaf diep en voorkom dat het
kwaad herrijst! ⛏️

*Datum & tijd: 22 en 23 maart 2025 12.00-17.30u bij 't Volbert in Enschede.*

**Entree is 10,00 voor spelers. Betaling volgt later via Tikkie.**

Wil je je reservering annuleren? Stuur ons dan een mailtje of appje.

### PS... Some good stuff!

- Leuk nieuws! We hebben voor dit event op zondag weer een jongerentafel
  beschikbaar (12-16 jaar). Ken je iemand die dit leuk zou vinden? Laat ze
  het weten!
- Met de nieuwe D&D editie die is uitgekomen in 2024, zitten we in een
  overgangsfase. Voor nu willen we spelers vragen om alleen met de
  2014 regels characters te maken. We laten je weten wat we met
  komende edities gaan doen.
  
### Wachtlijst

Alle plekken voor spelers zitten helaas vol, nu zijn er altijd wel een aantal afzeggingen, inschrijven voor de wachtlijst werkt dus!
We hebben nog wel dungeon masters nodig om op zaterdag of zondag een tafel te leiden, dus die kunnen nog steeds een plekje krijgen.