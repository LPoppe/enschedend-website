---
title: Vaults & Vampires
image: banner_05_vaults_and_vampires.jpg
---

Ben jij klaar voor het nieuwe avontuur van EnscheD&D? Wees welkom bij Vaults & Vampires! Geef je op via [{{ site.email }}](mailto:{{ site.email }}).

Entree: €7,50 (betaling vooraf per Tikkie)

Gezocht: avonturiers! Ditkeer zijn we in het ooit pittoreske dorpje Orthere. Maar er klopt iets niet. De huizen zijn leeg, ramen zijn dichtgespijkerd en de laatste mensen lijken uit het dorp te vertrekken. Ook de bank lijkt de dupe, en iedereen komt zijn goud en bezittingen halen om zijn geluk elders te zoeken. Wat er aan de hand is? Geef je nu op om het vervolg van dit spannende avontuur mee te maken, met JOUW character in de hoofdrol.
