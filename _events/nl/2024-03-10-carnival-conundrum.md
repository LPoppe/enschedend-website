---
title: The Carnival Conundrum
image: banner_13_carnival_conundrum.png
form: https://docs.google.com/forms/d/e/1FAIpQLSevujTld1fAosyTzNERcHSPbl2VnNum8lMJDo8f18nIQ5qZHQ/viewform
soldout: true
---

Goed nieuws: de inschrijvingen voor EnscheD&D zijn open! Dit keer reizen
we naar een magisch carnaval, waar jullie een zeer gewichtige taak
hebben gekregen van de Fantasy Board of Investigation: iets klopt niet
aan dit circus, en jullie moeten uitzoeken wat. Een van de weinige leads
is een poster die overal in de omgeving te zien is...

```
Bungling Bros’ Funfair and Festival Extraordinaire
For all your Dazzling Dreams
- Beazle and Bob
```

Voor dit avontuur kun je elke geprinte species of class spelen,
inclusief rare species zoals fairies, harengon en Loxodon.

*Datum & tijd: 10 maart 2024 12.00-17.30u bij 't Volbert in Enschede.*

Entree is 7,50 voor spelers.
