---
title: The Bad in the Belltower
image: banner_14_bad_in_the_belltower.png
form: https://docs.google.com/forms/d/e/1FAIpQLScJQBGcD5dtbHQaJQm-jAXoZYwSem7HgEJdNJQtFuf-08YDXg/viewform
soldout: true
---

**Goed nieuws: de inschrijvingen voor EnscheD&D zijn open! Kom mee met
ons op een avontuur in een mysterieus klooster, met oude traps en een
gevaarlijke draak... 🛕🐉**

In het stadje Goldpeak, omringd door prachtige, glooiende heuvels, was
het leven aangenaam. Nou ja, in ieder geval tot een paar weken geleden,
toen de mensen vreemde dingen begonnen te zien. Een dreigende,
gevleugelde schaduw zaait angst onder de dorpelingen. Sommige bewoners
zeggen dat ze scherpe tanden zagen glinsteren in het maanlicht, anderen
zagen vleermuisachtige vleugels, zo groot als de zeilen van een schip.
Een draak. Wat wil dit monster? Alle schatten van de inwoners, aanbeden
worden, of de volledige vernietiging van de omgeving?

De stedelingen hebben jouw party ingehuurd om te dealen met dit monster.
Het lijkt erop dat het verlaten Monastary of the Maiden de nieuwe
schuilplaats is geworden van het beest. De Monastery ligt aan de rand
van de Crumbling Cliffs, en om daar te komen wordt geen makkelijke
taak... Maar helden als jullie zijn, hebben jullie deze quest
geaccepteerd en zijn op reis gegaan om de goede mensen van Goldpeak te
redden.

*Datum & tijd: 30 juni 2024 12.00-17.30u bij 't Volbert in Enschede.*

**Entree is 7,50 voor spelers. Betaling volgt later via Tikkie.**

Wil je je reservering annuleren? Stuur ons dan een mailtje of appje.

### Wachtlijst

Het event is (al) uitverkocht. We zijn aan het kijken of we een volgende
gameday groter kunnen opzetten, daarvoor moeten we wel weten of er
voldoende animo is om de hogere kosten van een grotere locatie te
verantwoorden.

Daarnaast zijn er altijd een aantal afzeggingen, inschrijven voor de
wachtlijst werkt dus!

