---
title: To the Beacon and Beyond
image: banner_12_to_the_beacon_and_beyond.png
form: https://docs.google.com/forms/d/e/1FAIpQLSdkL-ps3tCVjZ0UdF41dhajt67xxXifz4xM8R97ktJgkLWKyQ/viewform
soldout: true
---

Goed nieuws: de inschrijvingen voor EnscheD&D zijn geopend! Deze keer
hebben we een spannend avontuur ter ere van World Space Week (4-10
oktober). Haal je mindflayer-vermomming uit de kast, pak je
ruimtevaartuig in, we gaan naar de ruimte, baby!

De astral sea bevat veel mysteries, adembenemende vergezichten en
evenveel gevaren. Je groep is op een missie aan boord van de Star
Striker, een klein schip waar je net uit de cryoslaap bent ontwaakt. Jij
en je gezelschap ontvangen een noodoproep op je FARRT (Find And Rescue
Radar Transponder) van een oude vriendin je hebt ontmoet in  eerdere
avonturen. Zonder na te denken zet je koers naar het enorme
ruimtestation waar ze wordt vastgehouden. Wat houdt haar daar? Daar kom
je wel achter wanneer aankomt...

Voor dit ruimte-avontuur kun je elke race of class spelen, maar als je
toevallig het Spelljammer-supplement bezit, passen de races en
backgrounds daaruit perfect bij dit avontuur.

**Datum & tijd: 8 oktober 2023 12.00-17.30u bij 't Volbert in Enschede.**

Entree is 7,50 voor spelers.
