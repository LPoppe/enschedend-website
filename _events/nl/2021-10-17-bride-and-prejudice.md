---
title: Bride & Prejudice
image: banner_06_bride_and_prejudice.jpg
form: https://docs.google.com/forms/d/e/1FAIpQLSdXJhiMU09tSc8KZPChMASIyUc38IgsoLpo0APdQO0YUyUkrg/viewform
soldout: true
---

Na een veel te lange pauze zijn we blij om eindelijk weer van start te kunnen gaan!

We hopen jullie allemaal weer te kunnen zien bij onze volgende Gameday: "Bride and Prejudice"!
