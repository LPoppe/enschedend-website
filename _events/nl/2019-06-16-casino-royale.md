---
title: Casino Royale
image: banner_03_casino_royale.jpg
---

Tijdens de EnscheD&D GameDay bevind je je in een rijdende trein, met aan boord passagiers, waarvan de een nog verdachter is dan de andere...

Schrijf je nu in voor de derde editie van de EnscheD&D GameDay: Casino Royale! Ontmoet nieuwe mensen, vertel het verhaal van jou karakter, en wie weet kom jij achter het mysterie dat zich bevindt aan boord van de trein!

Geef je snel op via [{{ site.email }}](mailto:{{ site.email }}). Het aantal plaatsen is beperkt, dus wacht niet te lang. Voor vragen kan je terecht op onze [FB pagina]({{ site.facebook }}).

16 juni 2019 12.00-17.30

Kaartjes €7,50 (onkosten locatie)
