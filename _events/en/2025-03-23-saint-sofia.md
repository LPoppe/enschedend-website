---
title: Saint Sofia and the 7 Spirits
image: banner_16_saint_sofia.png
form: https://docs.google.com/forms/d/e/1FAIpQLSelJgLaNcpNlWfi-Ipa67ww2xoKWuMogO3kKSUIkeC5P-EGhg/viewform
soldout: true
---

**Good news: registrations for EnscheD&D are open! And for the first
time ever, we are spreading the event over 2 days. It does come with
some extra puzzles to solve with how many tables we can run, so please
bear with us🐻 And now... Ready to join us on a new, regal adventure?
👑**

The party finds itself in the small tradehub of O’rethere, well known
for its apple orchards and splendid wildlife. But deep underneath the
local church, something stirs from its slumber, bringing with it threats
and riches to the surface. Grab your lanterns and pickaxes, and delve
deep to prevent something wicked from surfacing! ⛏️

*Date & time: March 22nd and 23rd 2025 12.00-17.30h at 't Volbert in Enschede.*

**Entrance is 10,00 for players. Payment follows later via Tikkie.**

Do you want to cancel your reservation? Please email or text us.

### PS... Some good stuff!

- Great news! On the Sunday we have a youth table available again for
  this event (age 12-16). Know someone who would enjoy this? Let them
  know!
- With the new D&D edition that came out in 2024, we are in a
  transition phase. For now, we want to ask players to create
  characters with only the 2014 rules. We will keep you posted on what
  we will do with future events.
  
### Waiting list

All player spots for the event have been filled, there are however always a number of cancellations, 
so signing up for the waiting list does work!
We are also still in need of dungeon masters to run the tables on saturday or sunday, 
so if you are interested in doing so. Please sign up and you will be able to get a spot.
