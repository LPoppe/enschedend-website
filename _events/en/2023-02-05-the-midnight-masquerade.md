---
title: The Midnight Masquerade
image: banner_10_midnight_masquerade.png
form: https://docs.google.com/forms/d/e/1FAIpQLSeX6a41Yzrabtd9CLVPREQG_V4BKXYl_T-5-U4wF9MdZpUp3w/viewform
soldout: true
---

Celebrate with us the 10th edition of EnscheD&D: the Midnight Masquerade! Get ready for the masquerade ball of your life. Extravagant guests, a beautiful palace, magical illusions… But how did you get here? And more importantly… How do you get out?

Midnight Masquerade is an adventure where you have one goal: to escape from the magical host who finds your group of adventurers a fitting entertainment in their palace. Nothing is what it seems.

**Date & time: February 5th 2023, 12.00-17.30 at the Volbert in Enschede!**

Entrance is 7,50 for players.
