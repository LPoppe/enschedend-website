---
title: The Tale of the Mad Mermaid
image: banner_08_mad_mermaid.png
form: https://docs.google.com/forms/d/e/1FAIpQLSdlcp-qf8uXkzQuXWRk3m8cJypgI-s3mbTc918GTiOf6_ZBgA/viewform
soldout: true
---

Majestic ships, open waters, the fresh sea air, uncharted islands, dangerous sea monsters, and of course... pirates!
Are you ready to board the Mad Mermaid for her next adventure?

**Date & time: 26 June 2022 12.00-17.30u at "Het Volbert" in Enschede!**

You can reserve your spot now! Payment will follow through the Tikkie app.
If you'd like to cancel your reservation, you can send us an e-mail or app.

Hopefully see you at the next Gameday!
