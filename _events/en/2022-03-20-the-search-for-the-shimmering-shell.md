---
title: The Search for the Shimmering Shell
image: banner_07_shimmering_shell.png
form: https://docs.google.com/forms/d/e/1FAIpQLSezJDepVgyxZRtPhRYzdTgX8PDSkgM1p9WsTM8Qsaa2pVEHQg/viewform
soldout: true
---

Get ready for some not quite Valentine's Day adventuring!

We hope to see you all at our next Gameday: "The Search for the Shimmering Shell"!
