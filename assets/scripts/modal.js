window.addEventListener('DOMContentLoaded', function(){
  var modals = document.getElementsByClassName('modal');
  var openLinks = document.getElementsByClassName('modal-open');
  var closeLinks = document.getElementsByClassName('modal-close');

  for (var i = 0, l = modals.length; i < l; ++i) {
    modals[i].addEventListener('click', function(event){
      if (event.target == this) {
        this.classList.remove('active');
      }
    });
  }

  for (var i = 0, l = openLinks.length; i < l; ++i) {
    openLinks[i].addEventListener('click', function(event){
      event.preventDefault();

      var name = this.dataset.modal;
      if (name) {
        var modal = document.getElementById(name + '-modal');
        if (modal) {
          modal.classList.add('active');
        }
      }
    });
  }

  for (var i = 0, l = closeLinks.length; i < l; ++i) {
    closeLinks[i].addEventListener('click', function(event){
      event.preventDefault();

      var name = this.dataset.modal;
      if (name) {
        var modal = document.getElementById(name + '-modal');
        if (modal) {
          modal.classList.remove('active');
        }
      } else {
        for (var i = 0, l = modals.length; i < l; ++i) {
          if (modals[i].contains(this)) {
            modals[i].classList.remove('active');
          }
        }
      }
    });
  }

  window.addEventListener('keyup', function(event){
    if (event.key == 'Escape') {
      for (var i = 0, l = modals.length; i < l; ++i) {
        modals[i].classList.remove('active');
      }
    }
  });
});
