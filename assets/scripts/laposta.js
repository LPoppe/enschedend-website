window.addEventListener('DOMContentLoaded', function(){
  var forms = document.getElementsByClassName('laposta-form');

  for (var i = 0, l = forms.length; i < l; ++i) {
    forms[i].action = 'https://' + forms[i].dataset.lapostaAction + '.email-provider.eu/subscribe/post/index.php';

    var fields = forms[i].getElementsByClassName('laposta-a');
    for (var ii = 0, ll = fields.length; ii < ll; ++ii) {
      fields[ii].name = 'a';
      fields[ii].value = forms[i].dataset.lapostaA;
    }

    var fields = forms[i].getElementsByClassName('laposta-l');
    for (var ii = 0, ll = fields.length; ii < ll; ++ii) {
      fields[ii].name = 'l';
      fields[ii].value = forms[i].dataset.lapostaL;
    }

    var fields = forms[i].getElementsByClassName('laposta-email');
    for (var ii = 0, ll = fields.length; ii < ll; ++ii) {
      if (fields[ii].tagName === 'LABEL') {
        fields[ii].htmlFor = forms[i].dataset.lapostaEmail;
      } else {
        fields[ii].name = forms[i].dataset.lapostaEmail;
        fields[ii].id = forms[i].dataset.lapostaEmail;
      }
    }

    var fields = forms[i].getElementsByClassName('laposta-name');
    for (var ii = 0, ll = fields.length; ii < ll; ++ii) {
      if (fields[ii].tagName === 'LABEL') {
        fields[ii].htmlFor = forms[i].dataset.lapostaName;
      } else {
        fields[ii].name = forms[i].dataset.lapostaName;
        fields[ii].id = forms[i].dataset.lapostaName;
      }
    }
  }
});
