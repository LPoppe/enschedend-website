# Bijdragen aan de website

Als je niet gewend bent aan code lezen, oogt dit project misschien wat intimiderend. Het is echter zo ingericht dat er
eigenlijk geen programmeerkennis nodig is om de website aan te passen, als je maar weet waar dingen staan.
Daar is deze beschrijving voor.

## Waar ben ik?

Waarschijnlijk thuis op een computer of laptop. Of misschien bekijk je dit ergens anders, op je mobiele telefoon,
tijdens een veel te lang durend gevecht met een bende goblins.

Hoe dan ook, je bent ook in Gitlab. Een plek waar met name broncode en tekst openbaar beschikbaar is in
versie-repositories. Dit helpt bij het gezamenlijk beheren, doorontwikkelen en updaten van onder andere websites.


## Hoe pas ik iets aan?

Een versie-repository bestaat uit een hoofdtak genaamd **master** en eventueel een aantal andere _branches_, wat
letterlijk aftakkingen van de master-tak zijn.

Binnen een _branch_ kun je aanpassen wat je wil, zonder dat dit (per ongeluk) direct de website aanpast.

Je kunt niet zomaar _branches_ maken in alle repositories, dat kan alleen in je eigen kopie, een zogenaamde _fork_. Na
het maken van je wijziging, maak je een _merge request_ om de beheerder op de hoogte te stellen van je wijzigingen.

Een aanpassing maken aan de website kun je daarom het best als volgt doen:

1. [Log in bij Gitlab](https://gitlab.com/users/sign_in?redirect_to_referer=yes), als je dit nog niet gedaan hebt.

2. **Maak (of ga naar) je eigen _fork_** door rechtsboven op de "Fork"-knop te drukken.

3. **Begin een nieuwe _branch_.** Dit doe je als volgt:
    * In de zijbalk zie je "Repository" staan, daaronder is een optie "Branches". Ga daar naartoe.
    * Op deze pagina zie je een knop "New branch", klik daarop.
    * Geef je branch een korte naam die het doel van je wijziging omschrijft.

4. Pas aan wat je wilt. Hieronder vind je instructies voor de meest gebruikelijke veranderingen.

5. **Maak een _merge request_ aan.** Dit werkt zo:
    * In de zijbalk zie je "Merge requests" staan, ga daar naartoe.
    * Op deze pagina zie je een knop "New merge request", klik daarop.
    * Selecteer hier de juiste _branches_ en klik op "Compare branches and continue"
    * Vul de velden in, voor zover nodig, en klik op "Create merge request"

6. Als de beheerder van de website je veranderingen goedkeurt, worden ze opgenomen in de master-branch.


### Een nieuw evenement plaatsen

Alle aankondigingen van evenementen vind je terug in de [events-map](_events), daarin zie je weer twee mappen
([en](_events/en) en [nl](_events/nl)]) voor de Engelse en Nederlandse versies van de aankondigingen.

De tekst van het evenement met de nieuwste datum komt op de voorpagina te staan en wordt gebruikt voor de banner van de
website.

Om een nieuw evenement toe te voegen, maak je in beide taal-mappen een bestand met als bestandsnaam
`YYYY-MM-DD-TITEL.md`, neem vooral een van de bestaande bestanden als voorbeeld.

Ook voor de inhoud kun je de bestaande bestanden als voorbeeld gebruiken. Algemene instructies
[vind je hier](https://jekyllrb.com/docs/posts). Belangrijk is dat het bestand begint met:

```
---
title: TITEL
image: NAAM_VAN_BANNER_AFBEELDING
form: WEB-ADRES_VAN_INSCHRIJFFORMULIER
---
```

De banner-afbeelding kun je toevoegen in [de images-map](assets/images). Let daarbij op de verhoudingen en het formaat
van de afbeelding. Het beste resultaat is over het algemeen met een PNG-afbeelding van 2500 × 624 pixels.


### Een post toevoegen

_Dit wordt (op dit moment 👀) nog niet actief gebruikt binnen de website, maar de basisfunctionaliteit is er. De
bijbehorende lijstpagina heeft `published: false` ingesteld, zodat deze niet op de website komt te staan. Ook worden
deze posts nog niet zichtbaar op de voorpagina._

De posts zijn bedoeld voor nieuws en andere updates. Ze staan in de [posts-map](_posts).
Instructies hiervoor [vind je hier](https://jekyllrb.com/docs/posts/).

Om de post beschikbaar te maken in het Engels en Nederlands, maak je twee versies van de post:

* Een post in de [en-map](_posts/en)
* Een post in de [nl-map](_posts/nl)

Dit zorgt ervoor dat het bericht op beide taal-versies van de website verschijnt.
Als je de Engelse versie weglaat, valt de website terug op de Nederlandse. Andersom werkt dit niet.


### Een pagina toevoegen

Naast events en posts kun je ook "gewone" pagina's toevoegen op de website. Deze vind je in de [en-map](en) en
[nl-map](nl).

Om de pagina beschikbaar te maken in het Engels en Nederlands, maak je in beide mappen een bestand. Vergeet daarbij niet
de `permalink` goed te zetten, zodat de pagina op de juiste plek op de website te zien is.

Verdere instructies [vind je hier](https://jekyllrb.com/docs/pages/).


### Overige teksten

De teksten in de header, footer en banner van de website zijn terug te vinden in de [data-map](_data). Per taal is er
een aparte map waar de relevante teksten in staan: ([en](_data/en/general.yml) en [nl](_data/nl/general.yml)).


### Opmaak van de website

Alle kleuren die de website gebruikt staan bij elkaar gegroepeerd in het [hoofd-CSS-bestand](css/main.scss).

Voor de objecten die de kleuren gebruiken staan onder de hoofdkleuren apart namen zoals 'buttons',
die een kleur uit de hoofdlijst gebruiken.

Wil je dan bijvoorbeeld de buttonkleur aanpassen, pas deze kleur dan aan. Wil je daar een nieuwe kleur voor gebruiken,
voeg die dan toe aan de kleurenlijst en gebruik een referentie aan die kleur. Bijvoorbeeld:

* Definieer de kleur: `$red:              #992b40;`
* Gebruik de kleur voor een object: `$button:           $red;`


### Afbeeldingen toevoegen

De bestanden van afbeeldingen horen in de [images-map](assets/images).
Let bij het gebruiken van een afbeelding op een aantal dingen:

* Het formaat van de afbeelding (lees: de bestandsgrootte).
  Hoe groter het bestand, hoe langer het duurt voordat deze te zien is.
* Het formaat van de afbeelding (lees: de lengte en breedte).
  Afbeeldingen op websites hoeven niet van drukwerkkwaliteit te zijn.
* Het formaat van de afbeelding (lees: het type).
  Voor foto's zijn JPEG-afbeeldingen goed, voor digitale tekeningen zijn meestal PNG-afbeeldingen beter.
